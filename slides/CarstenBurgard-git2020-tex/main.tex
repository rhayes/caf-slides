\documentclass{beamer}
\usetheme{NIKHEF}
\usepackage{adjustbox}
\usepackage{carstenstyle}
\usepackage{pifont}

\title{gitlab flow}
\subtitle{\acr{CAF} Tutorial 2020 -- Introduction}
\author{Carsten Burgard}
\coauthors{
  \vskip0pt
  \vspace{-1.5em}
  \includegraphics[height=2.5em]{logos/atlas}
  \hfill on behalf of the CAF development team\hfill
  \includegraphics[height=2.5em]{logos/plainroot}
}
\date{03.02.2020}

\setlogo{
  \includegraphics[height=2.5em]{logos/gitlogo}\par\vspace{0.5em}\par
  \includegraphics[width=\textwidth]{logos/footer_allInstitutes_feyn}
}

\begin{document}

\section{git}

\maketitle

\begin{frame}{git}
  \begin{itemize}
  \item \texttt{git} is the new recommended tool of version control
    \begin{itemize}
    \item much more flexible than \texttt{svn} (but sometimes more complicated)
    \item allows for various different workflows
    \item we recommend the \highlight{\href{https://docs.gitlab.com/ee/workflow/gitlab_flow.html}{\textit{gitlab flow}}} workflow (explained in this talk)
    \end{itemize}
  \item \texttt{git} is \textit{distributed} -- everyone has a full copy of the repository
  \end{itemize}
  \begin{block}{When you come from \texttt{svn}}
    \begin{itemize}
    \item \texttt{svn up} is now \texttt{git pull}
    \item \texttt{svn ci} has been split into
      \begin{itemize}
      \item \texttt{git add} -- announce (\textit{stage}) which files you want to commit
      \item \texttt{git commit} -- commits your changes \textit{locally}
      \item \texttt{git push} -- \textit{publishes} your changes 
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{\texttt{git} vs. GitLab}
  \begin{itemize}
  \item \texttt{git} is the version control software you run on your computer
  \item GitLab is the web page hosted at \texttt{gitlab.cern.ch}
    \begin{itemize}
    \item online (server-side) repository
    \item tracking issues and merge requests
    \item web interface to look at (and link to) code
    \item \dots
    \end{itemize}
  \item if you want to \texttt{git push} to a GitLab repository, you need to be able to \textbf{authenticate yourself}
    \begin{itemize}\scriptsize
    \item via \texttt{ssh} when using \texttt{git clone ssh://git@gitlab.cern.ch:7999}
      \begin{itemize}
      \item \textbf{recommended method for this tutorial}
      \item need an ssh key -- check for \texttt{\$HOME/.ssh/id\_rsa.pub}
      \item \highlight{\href{https://gitlab.cern.ch/profile/keys}{instructions}} to generate and upload
      \end{itemize}
    \item via \texttt{https} when using \texttt{git clone https://gitlab.cern.ch}
      \begin{itemize}
      \item \highlight{\href{https://gitlab.cern.ch/profile/gpg_keys}{instructions}} to generate and upload        
      \end{itemize}
    \item via Kerberos when using \texttt{git clone https://:@gitlab.cern.ch:8443}
      \begin{itemize}
      \item no key needed, but only works from some systems (\texttt{lxplus})
      \end{itemize}      
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Fork!}
  \includegraphics[width=\textwidth]{screenshots/fork}
  \vspace{-1em}
  \begin{itemize}
  \item forking a repository will create a personal copy of the repo 
  \item you should fork when you want to 
    \begin{itemize}
    \item start a new analysis package
    \item mess around with a package in private (without other people seeing what you do)
    \end{itemize}
  \item you will not do it as part of your every-day workflow
  \item it's useful to know how it works
  \item it will provide you with a playgroud for this tutorial!
  \item create a fork of \highlight{\href{https://gitlab.cern.ch/atlas-caf/CAFExample}{\texttt{CAFExample}}} now to try it out
  \end{itemize}
\end{frame}

\section{gitlab flow}

\begin{frame}{Create an issue}
  \begin{itemize}
  \item when you want to change something\dots
  \item start with creating an issue
  \end{itemize}
  \begin{columns}
    \begin{column}{.3\textwidth}
      \includegraphics[width=\textwidth]{screenshots/gitlab-menu}
    \end{column}
    \begin{column}{.8\textwidth}
      \begin{itemize}
      \item describe the study, bug or feature\par you want to work on
      \item submit the issue
      \end{itemize}
      \vspace{1em}
      \includegraphics[width=\textwidth]{screenshots/newIssue}
    \end{column}
  \end{columns}    
\end{frame}
\begin{frame}{Create an issue}
\includegraphics[width=\textwidth]{screenshots/submitIssue}
\end{frame}
\begin{frame}{Create a merge request}
  \begin{itemize}
  \item from your new issue, create a merge request to have your changes merged into master
  \item \textit{this can and should be done before you even start your work!}
  \end{itemize}
  \includegraphics[width=\textwidth]{screenshots/issue-createMR}
\end{frame}
\begin{frame}{Check out the branch}
  \begin{itemize}
  \item the merge request will automatically create a branch for you
  \end{itemize}
  \includegraphics[width=\textwidth]{screenshots/MR-branchname}
\end{frame}
\begin{frame}{Check out the branch}
  \begin{itemize}
  \item check out your branch with
    \begin{itemize}
    \item \texttt{git fetch}
    \item \texttt{git checkout 4-important-bug-in-myclass}
    \end{itemize}
  \item start your work, editing files, etc
  \item commit your work
    \begin{itemize}
    \item \texttt{git add theFileIedited}
    \item \texttt{git commit -m "the change I made"}      
    \end{itemize}
  \item \textbf{commit early, commit often!} your work will still be local and will not be visible to anyone yet, until you\dots
  \item \texttt{git push origin 4-important-bug-in-myclass}
  \end{itemize}
\end{frame}

\begin{frame}{Seeing your work online}
  \begin{itemize}
  \item you can see the changes online in your merge request
  \item \textit{gitlab} will try to compile and test the code automatically in the \textit{continuous integration (CI) pipeline}
  \end{itemize}
  \includegraphics[width=\textwidth]{screenshots/MR-pipeline}
\end{frame}

\begin{frame}{Continuous integration: The pipeline}
  \begin{itemize}
  \item the \acr{CI} will test your changes as soon as you push
    \begin{itemize}
    \item test if the code still compiles in a few pre-defined environments
      \begin{itemize}
      \item on an lxplus-like machine using some recent \acr{ASG} release
      \item standalone on an Ubuntu, just using plain \acr{ROOT}
      \end{itemize}
    \item execute some quick test analysis ($Z$+jets fake-factor analysis)
      \begin{itemize}
      \item do any errors occur?
      \item do the analysis results match what you would expect?
      \end{itemize}
    \end{itemize}
  \item if any of these tests fail, you will get an eMail:\par
    \begin{adjustbox}{width=\textwidth}\texttt{HWWAnalysisCode $\vert$ Pipeline \#615250 has failed for myBranch $\vert$ 1679d0c2 }\end{adjustbox}
  \item if you get this email, follow the link there\par
      \includegraphics[width=\textwidth]{screenshots/pipeline}\par
    \vspace{-1em}
  \item click on the \highlight{\ding{55}} to see the log of the job
  \item fix it, commit and push the fix
  \end{itemize}
\end{frame}

\begin{frame}[t]{When you're done}
  \begin{itemize}
  \item you can resolve the \textit{work-in-progress} (WIP) status
  \item authorized members can merge as soon as the pipeline succeeds
  \end{itemize}
\begin{overprint}[\textwidth]
  \only<1>{\includegraphics[width=\textwidth]{screenshots/MR-pipeline}}
  \only<2>{\includegraphics[width=\textwidth]{screenshots/MR-merge}}
  \only<3>{\includegraphics[width=\textwidth]{screenshots/MR-merge-final}}  
\end{overprint}
\end{frame}

\begin{frame}{Features of this workflow}
  \begin{itemize}
  \item \texttt{master} branch is protected, cannot be pushed to directly
  \item changing the \texttt{master} is always a conscious action by repository maintainer (developer, convener)
  \item the issues and merge requests remain in the system indefinitely, allowing to track all changes 
  \item always possible to see who is working on what (and what the status of that work is)
  \item everyone can work on their private branch without danger of disturbing anyone else
  \item collaborating on the same branch (and sharing work) is extremely easy
  \end{itemize}    
\end{frame}


\section{less fancy}
\begin{frame}{Why so fancy?}
  \begin{itemize}
  \item this workflow assumes that you have internet access, and like graphical interfaces
  \item if this is not true, the same workflow can also be achieved from the console (without \texttt{gitlab} interaction)
  \item start with creating your branch using\par\texttt{git checkout -b my-new-branch}
  \item make your changes, commit and push your work (as you would do otherwise)
  \end{itemize}    
\end{frame}

\begin{frame}{Why so fancy?}
  \includegraphics[width=\textwidth]{screenshots/createMR}
  \vspace{-1.8em}
  \begin{itemize}
  \item create a merge request for your branch online
  \end{itemize}    
  \includegraphics[width=\textwidth]{screenshots/createMR-menu}
  \vspace{-1.8em}
  \begin{itemize}
  \item maintainers can also merge from the console using\par
    \texttt{git checkout master \&\& git merge my-new-branch}
  \end{itemize}    
\end{frame}

\section{Conclusions}
\begin{frame}{Conclusions}
  \begin{itemize}
  \item the age of \texttt{svn} is over, the age of \texttt{git} has begun
  \item the \textit{gitlab flow} workflow allows us to
    \begin{itemize}
    \item share the same repository for all collaborating analyzers
    \item have a clear, defined, protected, official \texttt{master} state at any point in time
    \item allow each analyzer to use version control for their own work
    \item make everyones work available to everyone else without conflicts
    \end{itemize}
  \item let's harvest the power of \texttt{git}!
  \end{itemize}
  \par\centering
    \includegraphics[height=2.5em]{logos/gitlogo}
\end{frame}


\end{document}

