\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usetheme{NIKHEF}
\usepackage{carstenstyle}
\usepackage{adjustbox}
\usepackage{emacslstdefaults}
\usepackage{pxfonts}
\definecolor{folder} {HTML}{5555FF}
\definecolor{samplefolder}{HTML}{55FF55}
\definecolor{hidden} {HTML}{FF55FF}
\definecolor{tag} {HTML}{00AAAA}
\providecommand{\tabularnewline}{\\}
\newcommand\hindent{\hspace{2em}}
\newcommand\stylefolder[1]{{\bfseries\color{folder}#1}}
\newcommand\stylesamplefolder[1]{{\bfseries\color{samplefolder}#1}}
\newcommand\styletag[1]{{\color{tag}#1}}
\newcommand\stylehidden[1]{{\bfseries\color{hidden}#1}}

\title{SFramework}
\subtitle{Statistics with \texttt{CAF}}

\author{Carsten Burgard}

\setlogo{
  \includegraphics[width=\textwidth]{logos/footer_allInstitutes_feyn}
%  \includegraphics[height=3.5em]{logos/ATLAS-Logo-Ref-RGB}  \hspace{1em}
%  \includegraphics[height=3.5em]{logos/caf-logo.png}\par
%  \includegraphics[height=2.5em]{NIKHEF-logo}
%  \hspace{1em}
%  \includegraphics[height=2.5em]{logos/ou-logo}
%  \hspace{1em}
%  \includegraphics[height=2.5em]{logos/alu.png}
%  \hspace{1em}
%  \includegraphics[height=2.5em]{logos/simon-fraser-university-sfu-logo2}
}

\tikzset{arrow/.style={->,shorten <=1mm,shorten >=1mm}}

\begin{document}

\section{Introduction}

\maketitle

\begin{frame}{Introduction}
  \begin{itemize}
  \item in the last 36 hours, you have learned how to
    \begin{itemize}
    \item produce a sample folder
    \item with histograms and counters
    \item for your nominal analysis
    \item and systematic variations
    \end{itemize}
  \item how can you use that to setup a fit? \par
    $\Rightarrow$ \texttt{SFramework} is the in-house statistics solution
  \end{itemize}
  \begin{block}{Scope}
  \begin{itemize}
  \item this is \textbf{not} an introduction to statistical methods in \acr{HEP}\par
    \textit{you can find that \highlight{\href{https://indico.desy.de/indico/event/19085/timetable}{elsewhere}}}
  \item this will guide you through the neccessary steps to setup a \acr{RooFit}-based workspace \& fit based on your \acr{CAFCore} analyis
  \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Hands-on Setup}
  \begin{itemize}
  \item no specific requirements of release or \acr{ROOT} version
  \item runs with standalone \acr{ROOT} or \acr{ASG} release\par
    \textit{opportunity to see if things work on your laptop?}
  \end{itemize}
  \begin{block}{Minimial Setup}
    \vspace{-1em}
    \begin{adjustbox}{width=\textwidth,minipage=20cm}
    \begin{lstlisting}[language=bash,numbers=none]
mkdir AnalysisExample; cd AnalysisExample
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-caf/CAFExample.git
cd CAFExample/CAFCore; git checkout master; cd -
mkdir build; cd build
cmake ../CAFExample
source ../CAFExample/setup/setupAnalysis.sh
make -j4
    \end{lstlisting}
    \end{adjustbox}
  \end{block}
  \begin{block}{Advanced Setup ($H\to WW$ example)}
    same for \texttt{atlas-physics/higgs/hww/HWWAnalysisCode}
  \end{block}  
\end{frame}

\begin{frame}[fragile]{General Workflow}
  \begin{tikzpicture}[y=2cm,x=3cm]
    \node (hist) at (0,-1) {\texttt{histograms}};
    \node at(1,-1) {\includegraphics[width=2cm]{plots/SR}};
    \node (ws) at (0,-2) {\texttt{workspace}};
    \node at (1,-2){\begin{adjustbox}{width=2cm,minipage=60cm}\lstinputlisting{plots/wsprint}\end{adjustbox}};    
    \node (results) at (0,-3) {results};
    \node at(1,-3) {\includegraphics[width=2cm]{plots/likelihood}};
    \draw [arrow] (hist) -- (ws);
    \draw [arrow] (ws) -- (results);
  \end{tikzpicture}
\end{frame}

\begin{frame}{General Workflow -- comments}
  \begin{itemize}
  \item this workflow conceptually splits the process in two parts
    \begin{itemize}
    \item building a statistical model of your data (\texttt{Pdf})
    \item fitting that model to the data by minimizing the $-\!\log(L)$
    \end{itemize}
  \item in-line with \acr{ATLAS} practices, for example, Higgs combination
    \begin{itemize}
    \item analysers give workspace to HComb
    \item fit is run by people in HComb
    \end{itemize}
  \item workspace building is completely decoupled from the fitting itself, can happen with different frameworks/tools
    \begin{itemize}
    \item use \texttt{SFramework} to build your workspace\par\footnotesize
      \textit{highly encouraged: \texttt{SFramework} can re-use much of the existing information in your sample folder!}
    \item use any fitting tool you like (but also \texttt{SFramework}) to run your fit\par\footnotesize
      \textit{\texttt{SFramework} provides many features and convenient configuration, but other statistics frameworks are also very popular!}
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Workspace building}

\begin{frame}{Workspace building with \texttt{SFramework}}
  is segmented into several distinct tasks (or \texttt{Actions})
  \begin{enumerate}
  \item \textbf{define your model}
    \begin{itemize}
    \item Which signal and background samples exist? Include data?\par
      $\rightarrow$ \textit{paths in your sample folder}
    \item Which regions/channels do we include in the fit?\par
      $\rightarrow$ \textit{cuts and histogram names in your sample folder}
    \item Which systematic variations exist, and how are they defined?\par
      $\rightarrow$ \textit{experimental systematics included in your sample folder}
    \end{itemize}
  \item post-process your model (optional)
    \begin{itemize}
    \item prune and sanitize your systematics
    \item include additional theory systematics
    \item more fancy things
    \end{itemize}
  \item \textbf{create a workspace} from your model
  \item post-process the workspace (optional)
    \begin{itemize}
    \item reparametrize parts of your Pdf
    \item rename, fix, remove or merge parameters
    \end{itemize}
  \item \textbf{write out the workspace} to a file
  \end{enumerate}
\end{frame}


\begin{frame}[fragile]{Primer: \texttt{Actions}}
  \begin{itemize}
  \item \texttt{SFramework} is steered with config files (surprise!)
  \item the config files have the \texttt{TQFolder} syntax (next surprise!)
    \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]
# -*- mode: tqfolder -*-
+DoSomething {
  +WithObject {
    # ... how to do it ...
  }
}
+DoTheNextThing {
  +WithObject;
}
  \end{lstlisting}
\item use TQFolder syntax to compactify your config files!
\item actions executed in order given -- for two calls to the same action, suffix it with \texttt{.somename} (\texttt{+DoSomething.first})
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Step 1: \texttt{CreateModels} -- mini-example}
  \vspace{-3em}
  \begin{lstlisting}[language=tqfolder]
+CreateModels {
  +MyAnalysis{ # model name
    +Samples {
      +Signal     { <Type=S, Path="sig"> }
      +Background { <Type=B, Path="bkg"> }      
    }
    +Channels {
      +SR {  <Histogram = "CutSR/MyVar">  }
      +CR {  <Counter = "CutCR">          }
    }
    +Variations {
      <SampleFolder="samples.root:samples">
      +Nominal {  <variation=""> }
    }
  }
}
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Sample folder layout}
  \begin{center}
    $\Rightarrow$ Download your sample folder \highlight{\href{https://indico.cern.ch/event/771763/contributions/3246713/attachments/1767912/2871306/samples.root}{here}} $\Leftarrow$
  \end{center}
  \begin{adjustbox}{width=\textwidth}
  \begin{tabular}{l l l}
\bfseries Name & \bfseries Class & \bfseries Details\tabularnewline\hline\hline
\stylesamplefolder{sig}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 50 +/- 0.707107, N=5000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1 +/- 0.0001 [raw: 0]\tabularnewline
\stylesamplefolder{bkg}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 500 +/- 7.07107, N=5000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1000 +/- 1 [raw: 0]\tabularnewline
\stylesamplefolder{data}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 553 +/- 23.516, N=10000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1000 +/- 31.6228 [raw: 0]\tabularnewline
  \end{tabular}
  \end{adjustbox}
  \end{frame}

\begin{frame}[fragile]{Step 1: \texttt{CreateModels} (cont'd)}
  \begin{itemize}
  \item this model is extremely minimal
    \begin{itemize}
    \item only two samples (\texttt{Signal}, \texttt{Background})
    \item no systematics are added (not even \acr{MC} stat uncertainties)
    \item no parameters -- not even \acr{NF}s (\textbf{cannot fit this yet!})
    \end{itemize}
  \item let's add a normalization factor
    \vspace{-2em}
    \begin{lstlisting}[language=tqfolder,numbers=none]
+Signal {      
  <Type=S, Path="sig">
  +NormFactor.mu {
    <Val=1.,Low=-50,High=50.,Const=false>
  }
}
    \end{lstlisting}
  \item let's also add \acr{MC} stat uncertainties for background
    \vspace{-2em}
    \begin{lstlisting}[language=tqfolder,numbers=none]    
+Background {
 <Type=B,Path="bkg",ActivateStatError=true>
}
    \end{lstlisting}
    \item can you also add a background NF?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Primer: Run it!}
  \begin{block}{Execute}
    \texttt{python statistics.py myconfig.txt}
  \end{block}
  \begin{block}{Output}
    \vspace{-1em}
  \begin{adjustbox}{minipage=20cm,width=\textwidth}
\begin{lstlisting}[numbers=none]
starting statistical treatment of the analysis results
INFO: Imported config file 'myconfig.txt'
INFO: Starting Statistics Manager on 'myconfig.txt'
INFO in TSStatisticsManager::run(...) :  === CreateModels: CreateModels === 
SFramework/TSStatisticsManager: Creating model 'MyAnalysis' 
SFramework/TSModelBuilder: prepareModel(): Preparing model...
SFramework/TSModelBuilder: createDefinition(): Creating definition for model 'MyAnalysis' ...
SFramework/TSModelBuilder: collectAllHistograms(): Start collecting histograms...
SFramework/TSModelBuilder: collectHistograms('Nominal'): Collecting histograms...
SFramework/TSSystematicsManager: includeAllSystematics(): Start including systematics ...
SFramework/TSSystematicsManager: processAllSystematics(): Start processing systematics matching '*'
INFO: Done processing 'myconfig.txt'!
\end{lstlisting}
  \end{adjustbox}
  \end{block}
\end{frame}


\begin{frame}[fragile]{What about the workspace?}
  \begin{itemize}
  \item we didn't actually build a workspace yet -- just a model
  \item we need to turn the model into a workspace
  \end{itemize}
    \vspace{-2em}
\begin{lstlisting}[language=tqfolder,numbers=none]    
+CreateWorkspaces {
  +MyAnalysis; # model & workspace name
}

+ExportWorkspaces {
  +MyAnalysis { # workspace name
    <outputFile="workspace.root">
  }
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Did it work?}
  \begin{block}{Open root\dots}
    \vspace{-3em}
    \begin{lstlisting}[language=bash,numbers=none]
      root workspace.root
    \end{lstlisting}
  \end{block}
  \begin{block}{Run a simple fit}
    \begin{adjustbox}{minipage=15cm,width=\textwidth}    
    \vspace{-2em}
      \begin{lstlisting}[language=rootc++,numbers=none]      
        RooAbsPdf* pdf = MyAnalysis->pdf("simPdf")
        RooAbsData* data = MyAnalysis->data("asimovData")
        pdf->fitTo(*data)
      \end{lstlisting}
    \end{adjustbox}
    \begin{itemize}
      \item that was easy! but how do we run a fit with \texttt{SFramework}?
    \end{itemize}
  \end{block}
\end{frame}

\section{Running fits}

\begin{frame}[fragile]{Running Fits with \texttt{SFramework}}
  \begin{itemize}
  \item let's run a fit with SFramework
  \end{itemize}
  \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]
+ImportWorkspaces {
 +MyAnalysis { <inputFile="workspace.root"> }
}
+CalculateSignificance {
  +MyAnalysis{
    +asimov { <dataset="asimovData">  }    
  }
}
  \end{lstlisting}
  \vspace{-1em}
  \begin{block}{Hands-on: Addition}
    \begin{itemize}
      \item for a meaningful significance, add a \acr{POI} to your model
    \end{itemize}
    \vspace{-3em}
  \begin{lstlisting}[language=tqfolder]
@CreateModels/?{ +Parameters{ <POI="mu"> } }
  \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{How about a likelihood scan?}
  \begin{itemize}
  \item profile likelihood scans are profiled, that is,
    \begin{itemize}
    \item a likelihood is scanned over a certain range in the \acr{POI}
    \item for each point, the \acr{POI} is fixed to the coordinates of that point
    \item all other parameters are fitted to the data
    \item value of the likelihood after minimization is shown as a function of the \acr{POI} value
    \end{itemize}
  \item can be vey time-consuming to run locally
  \item but for our simple model, this should run within seconds!
  \end{itemize}
  \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]  
+ScanLikelihood.mu{
  +MyAnalysis {
    <datasetName="asimovData">
    +mu{
      <min=0,max=3.5, nbins=100>
    }
  }
}
  \end{lstlisting}
\end{frame}


\section{Results}

\begin{frame}[fragile]{How do we get the results?}
  \begin{itemize}
  \item just "running a fit" is not so useful
  \item we want to visualize the results
  \item \texttt{SFramework} stores all the results in a TQFolder
  \begin{lstlisting}[language=tqfolder]      
+ExportResults {
  +MyAnalysis {
    <outputFile = "result.root">
  }
}
  \end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Plotting the Likelihood?}
  \begin{itemize}
  \item you can pick up the result and plot it
  \item there is an action for this
  \end{itemize}
  \begin{adjustbox}{minipage=18cm,width=\textwidth}
  \begin{lstlisting}[language=tqfolder]      
+ImportResults {
  +MyAnalysis {
    <inputFile = "result.root:MyAnalysis">
  }
}
+PlotLikelihoodScan {
 +MyAnalysis {
  <arrangement = "$TQPATH/../SFramework/share/templates/PlotLikelihoodScanSignificance.txt">
  <outputFile="likelihood.pdf">
  +mu_asm {
    <source = "LikelihoodScans/ScanLikelihood.mu/Scan">
    <style.lineColor=kRed, style.title="#mu">
  }
  <style.title.xAxis = "#mu (profiled)">
 }
}
  \end{lstlisting}
  \end{adjustbox}
\end{frame}

\begin{frame}{Plotting the Likelihood}
  \includegraphics[width=\textwidth]{plots/likelihood}
\end{frame}

\section{Recap \& Hands-on}

\begin{frame}{Recap}
  \begin{itemize}
  \item \texttt{SFramework} knows 3 types of objects:
    \begin{description}
    \item [model] TQFolder encoding a statistical model
    \item [workspace] A \texttt{RooFit} workspace
    \item [result] TQFolder encoding a statistical result
    \end{description}
  \item \texttt{SFramework} performs \texttt{Action}s on these objects
    \begin{itemize}
    \item can take objects as inputs
    \item can produce objects as outputs
    \item can have side events (files written, etc.)
    \end{itemize}
  \item ``Is there an \texttt{Action} that can \dots''
    \begin{itemize}
      \item very likely answer: yes, see \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/index.html}{documentation}}
    \end{itemize}
  \end{itemize}
  \begin{block}{Hands-on: Tasks}
    \begin{itemize}
    \item can you reproduce the Likelihood scan? 
    \item how large is the expected significance of this toy analysis?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{FAQ \& Gotchas}
  \begin{itemize}
  \item you only need the \texttt{Import...} and \texttt{Export...} actions if you actually split your configuration into different files
    \begin{itemize}
      \item if you use them nevertheless, you will see error messages, but everything will still run as intended
    \end{itemize}
  \item if your significance is $0.0$, make sure you have the \texttt{POI} tag set on the \texttt{Parameters} folder
  \item you need to add the background normalization factor for the likelihood scan to converge -- the result should be $1.8$
    \begin{itemize}
    \item if you forget this, you will get error messages about \texttt{inf} values of the likelihood and the scan will be empty
    \item make sure that you give a unique name ot the background normalization factor -- don't call it \texttt{mu}!
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Reality Check}

\begin{frame}{That was easy, what now?}
  \begin{itemize}
  \item in reality, things are often more involved
  \item big factor: systematics
  \item inputs get very large, model building becomes slow\par
    $\Rightarrow$ split your config into steps to minimize reprocessing
  \item fits become very slow\par
    $\Rightarrow$ parallelize using a batch system, merge back results
  \item let's take a look at a few realistic examples
    \begin{itemize}
      \item $H\to WW$ \acr{VBF} fit (simplified)
    \end{itemize}
  \end{itemize}
  \begin{block}{Hands-on}
    \begin{itemize}
  \item use the \texttt{HWWAnalysisCode} setup
  \item execute the steps as we go along
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{\acr{VBF} overview}
  \begin{itemize}
  \item fitting split into several steps
  \begin{description}
  \item[build the model] \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildModel.txt}{buildModel.txt \ExternalLink}
  \item[build the workspace] \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildWorkspace.txt}{buildWorkspace.txt \ExternalLink}
  \item[run a preliminary fit] \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/runFit.txt}{runFit.txt \ExternalLink}
  \item[\textit{batch submission}] submit jobs to batch system
    \par on your laptop, parallelize with \textit{local controller}
  \item[collect the results] \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/collectResults.txt}{collectResults.txt \ExternalLink}        
  \item[plot the results] \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/plotResults.txt}{plotResults.txt \ExternalLink}    
  \end{description}
\item config files located in \texttt{share/config/statistics/VBF}
\item execute each one by calling \texttt{statistics.py /path/to/the/file.txt}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through: \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildModel.txt}{buildModel \ExternalLink}}
  \begin{itemize}
  \item \texttt{CreateModel} has nothing new, except a \texttt{Parameters} and \texttt{ParamSettings} block to configure some parameters and meta-information
  \item some things are missing, and will be added \textit{a-posteriori}
    \begin{itemize}
    \item add signal samples from \highlight{\href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/tree/master/share/config/statistics/common/snippets}{external files}} for easy switching of \acr{STXS} scheme
      \vspace{-1.2em}\par
   \begin{adjustbox}{width=\textwidth,minipage=20cm}
    \begin{lstlisting}[language=tqfolder,numbers=none]
$include("config/statistics/common/snippets/samples.noSTXS.txt");
<POI = {"muVBF"}> @ CreateModels/?/Parameters;
    \end{lstlisting}
   \end{adjustbox}
      \item add data separately -- blind by commenting this block
      \vspace{-1.2em}\par
   \begin{adjustbox}{width=\textwidth,minipage=20cm}    
    \begin{lstlisting}[language=tqfolder,numbers=none]
+CreateModels/HWWVBF/Samples {
  +Data {
    <Type = D, Path = "data/$(channel)">
  }
}
    \end{lstlisting}
   \end{adjustbox}
  \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through: \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildModel.txt}{buildModel \ExternalLink} (cont.)}
  \begin{itemize}
  \item experimental systematics are included from \highlight{\href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/tree/master/share/config/statistics/common}{external files}}
    \vspace{-1.2em}\par
   \begin{adjustbox}{width=\textwidth,minipage=20cm}        
    \begin{lstlisting}[language=tqfolder,numbers=none]
+CreateModels/HWWVBF{
  $include("config/statistics/common/systematics-cp.txt");
  $include("config/statistics/common/systematics-ff.txt");  
  <IsOverallSys=true, ConstraintType = "Gaussian"> @ Systematics/?;
  <IsHistoSys=false> @ Systematics/ATLAS_*,Systematics/HWW_FakeFactor_*;
  <Fallback = "Nominal"> @ Variations/?;
}
    \end{lstlisting}
   \end{adjustbox}
   \texttt{Fallback} option allows usage of sparse sample folders \par $\Rightarrow$ missing systematic variations will be replaced by nominal
 \item add lumi uncertainty separately -- global scaling factor on all samples not normalized from data
    \vspace{-1.2em}\par
   \begin{adjustbox}{width=\textwidth,minipage=20cm}        
    \begin{lstlisting}[language=tqfolder,numbers=none]
+CreateModels/?/Samples{
  @? {
    $escape(Wjets);
    ...

    +OverallSys.ATLAS_LUMI_2017 {
      <Val = 1., Low = 0.98, High = 1.02>
    }
  }
}
    \end{lstlisting}
   \end{adjustbox}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildModel.txt}{buildModel \ExternalLink} (cont.)}
  \begin{itemize}
  \item activate \acr{MC} statistical uncertainty for background samples
    \vspace{-1.2em}\par
   \begin{adjustbox}{width=\textwidth,minipage=20cm}        
    \begin{lstlisting}[language=tqfolder,numbers=none]    
+CreateModels/?/Samples{
  @? {
    <ActivateStatError = true, NormalizeByTheory = false> @ ?;
    <ActivateStatError = false, NormalizeByTheory = false> @ sig*;
  }
}
    \end{lstlisting}
   \end{adjustbox}
 \item write out final version of this config for bookeeping reasons
    \vspace{-1.2em}\par
    \begin{adjustbox}{width=\textwidth,minipage=20cm}        
     \begin{lstlisting}[language=tqfolder,numbers=none]   
<saveConfig="./workspaces/runVBF-$(fitLabel)/buildModelConfig.txt">
    \end{lstlisting}
   \end{adjustbox}
 \item switch to easily redirect the outputs to a different folder
   \vspace{-1.2em}\par
    \begin{adjustbox}{width=\textwidth,minipage=20cm}        
    \begin{lstlisting}[language=tqfolder,numbers=none]   
$replace("*:*",fitLabel="test");
    \end{lstlisting}
    \end{adjustbox}
    \par \textbf{very} useful -- alternatively, can also use
    \vspace{-1.2em}\par
    \begin{adjustbox}{width=.9\textwidth,minipage=18cm}
      \begin{lstlisting}[numbers=none]
        statistics.py ... --change '$replace("*:*",fitLabel="test");'
      \end{lstlisting}
    \end{adjustbox}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/buildWorkspace.txt}{buildWorkspace \ExternalLink}}
  \begin{itemize}
  \item start by importing model
  \item nothing new here
  \item can include post-processing
    \begin{itemize}
    \item apply general edits \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionEditModels.html}{EditModels}}
    \item rename parameters in the workspace: \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionEditWorkspaces.html}{EditWorkspaces}}
    \end{itemize}
  \item can include theory systematics
    \begin{itemize}
    \item dedicated action \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionImportSystematics.html}{ImportSystematics}}
    \end{itemize}
  \item many more actions available (sanitize systematics, \dots)
  \item can become quite elaborate, see e.g. ggF \highlight{\href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/ggF/editModel.txt}{editModel}}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/runFit.txt}{runFit \ExternalLink}}
  \begin{itemize}
  \item significance calculation \texttt{CalculateSignificance}
  \item can configure fit with lots of options
    \begin{itemize}
    \item \lstinline[language=tqfolder]|<fit.runHesse=true>| run hesse (correlation matrix)
    \item \lstinline[language=tqfolder]|<fit.runMinos=true>| activate minos calculation of asymmetric uncertainties on certain parameters
      \lstinline[language=tqfolder]|fit.runMinosVars = {"*ATLAS_*","*HWW_*","*mu*"}>| 
    \item \lstinline[language=tqfolder]|<fit.startingStrategy=2| set the starting strategy  
    \item \lstinline[language=tqfolder]|<fit.presetParam.X, fit.initParam.X>|  set initial values
    \item \lstinline[language=tqfolder]|<fit.verbose=1, fit.logToFile="myfile.log">| control verbosity and log file
    \end{itemize}
  \item these (and more) \texttt{fit.*} options work on every fitting action
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through: \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/runFit.txt}{runFit \ExternalLink} (cont.)}
  \begin{itemize}
  \item likelihood scan (just like for minimal example)
  \item realistic fits take longer $\Rightarrow$ parallelize on a batch system
  \item syntax features configure asimov \& observed fits (1D \& 2D)
  \item using the \texttt{WriteScanLikelihood} action (instead of \texttt{ScanLikelihood}) will write out config-files for batch submission instead of running the fit
  \item same options available, plus a few more, specifically  \par
    \begin{adjustbox}{width=\textwidth,minipage=15cm}{\lstinline[language=tqfolder]|<outputPath = "./workspaces/runVBF-$(fitLabel)/Batch/Scans/obsVBF">|}\end{adjustbox}
  \item performing this action will fill the directory with config files
  \item to submit everything to batch, call\par\begin{adjustbox}{width=\textwidth}\texttt{submit-statistics.py /workspaces/runVBF-test/Batch/Scans/obsVBF ...}\end{adjustbox}
  \item results will be collected later
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Small tangent: Breakdowns}
  \begin{itemize}
  \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionCalculateBreakdown.html}{calculate the breakdown}} of systematic uncertainties
    \begin{itemize}
    \item run one fit per systematic uncertainty
    \item see how the total uncertainty on the \acr{POI} changes when the systematic is removed (set to constant)
    \end{itemize}
  \item can take very long $\Rightarrow$ \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionWriteCalculateBreakdown.html}{batch-parallelization}} available
  \end{itemize}
  \vspace{-1.5em}
  \begin{adjustbox}{width=\textwidth,minipage=20cm}        
    \begin{lstlisting}[language=tqfolder,numbers=none]       
      +CalculateBreakdown {
        +HWWVBF {
          +asimov {
            <datasetName="asimovData_1", invert=false>
            <group.MCSTAT = "*gamma_stat_*"> #,"external*">
            <group.SYS = "*ATLAS_*", config.group.SYS.except = "ATLAS_norm*">
            <singles.SYS = "*ATLAS_*">
            <group.STAT = "*", config.group.STAT.invert = true>
            <group.NORM = "*ATLAS_norm*">
            <singles.NORM = "*ATLAS_norm*">
            <group.FAKE = "*HWW_FakeFactor_*">
            <singles.FAKE = "*HWW_FakeFactor_*">
          }
        }
      }
    \end{lstlisting}
  \end{adjustbox}
  \vspace{-1em}
  \begin{itemize}
  \item can you understand how the \texttt{TQFolder} syntax is used to stitch together these action configs?
  \item alternative calculation method: \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionCalculateBreakdown.html}{impacts}}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/runFit.txt}{runFit \ExternalLink} (cont.)}
  \begin{itemize}
  \item writing out the post-fit workspace
  \item exporting the preliminary results\par not including the results from batch yet
  \item additional action: \texttt{PrintResults} to print results
    \begin{itemize}
    \item printing to console
    \item also creating a \LaTeX{} file
    \end{itemize}
  \end{itemize}
  \begin{block}{Hands-on}
    \begin{itemize}
    \item did you submit your jobs yet? did it work?
    \item did your jobs finish already?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/collectResults.txt}{collectResults \ExternalLink}}
  \begin{itemize}
  \item various \texttt{Collect} actions to collect the results from the different batch tasks
    \begin{itemize}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionCollectBreakdown.html}{CollectBreakdown}}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionCollectBreakdown.html}{CollectImpacts}}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionCollectBreakdown.html}{CollectScanResults}} (multiple instances)
    \end{itemize}
  \item running this step can take surprisingly long, as the result files from the individual jobs can be large
  \item should complete within about a minute
  \item writing out the collected (merged) results to a new result file
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\acr{VBF} Walk-through:  \href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/blob/master/share/config/statistics/VBF/plotResults.txt}{plotResults \ExternalLink}}
  \begin{itemize}
  \item various actions you already know
    \begin{itemize}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionImportResults.html}{ImportResults}}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionImportWorkspaces.html}{ImportWorkspaces}}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionPlotLikelihoodScan.html}{PlotLikelihoodScan}}      
    \end{itemize}
  \item some new ones
    \begin{itemize}
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionPlotCorrelations.html}{PlotCorrelations}} -- make a 2D plot of the correlation matrix
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionExportHistograms.html}{ExportHistograms}} -- write out a \texttt{.root} file with histograms from some state of the workspace (useful for postfit plots)
    \item \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/ActionPlotResults.html}{PlotResults}} -- write out a parameter plot (possibly including breakdown, pulls and constraints) -- the \texttt{...TikZ} version will write the plot as \texttt{TikZ} code
    \end{itemize}
  \end{itemize}    
\end{frame}

\section{Conclusions}

\begin{frame}{Summary \& Conclusions}
  \begin{itemize}
  \item \texttt{SFramework} is a powerful, versatile statistics framework
    \begin{itemize}
    \item native, smooth integration in \acr{CAFCore}
    \item no specific dependencies
    \item wealth of actions for workspace building, fitting and plotting
    \end{itemize}
  \item configuration using the \texttt{TQFolder} syntax
  \item everything implemented as \texttt{Actions}
    \begin{itemize}
    \item if you are missing anything, you can easily add your own!
      \item documentation available \highlight{\href{https://atlas-caf.web.cern.ch/SFrameworkActions/index.html}{online}}
    \end{itemize}
  \item \texttt{SFramework} uses \texttt{HistFactory} as a backend -- workspaces are fully standardized and compatible with every other framework out there
  \end{itemize}
\end{frame}

\begin{frame}{Outlook}
  \begin{itemize}
  \item with \texttt{SFramework}, you should be able to setup and run the fit for most standard analyses
  \item some features are not (yet) supported:
    \begin{itemize}
    \item unbinned fits
    \item limit calculation \& plots
    \end{itemize}
  \item some additional packages can be integrated to provide bonus functionality
    \begin{itemize}
    \item improve your fit stability and get additional workspace manipulation magic by including \highlight{\href{https://gitlab.cern.ch/cburgard/RooFitUtils}{RooFitUtils}}
    \item get additional \texttt{RooFit} classes to upgrade your setup to a full \texttt{HComb ROOT} release by including \highlight{\href{https://gitlab.cern.ch/cburgard/RooFitExtensions}{RooFitExtensions}}
    \item perform \acr{EFT} analyses using \highlight{\href{http://cds.cern.ch/record/2143180/files/LHCHXSWG-INT-2016-004.pdf}{Effective Lagrangian Morphing}} by using \highlight{\href{https://gitlab.cern.ch/cburgard/RooLagrangianMorphing}{RooLagrangianMorphing}} and \highlight{\href{https://gitlab.cern.ch/atlas-caf/CAFELM}{CAFELM}}
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}
