\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usetheme{tudo}
\usepackage{listings}
\usepackage{carstenstyle}
\usepackage{tikz,pgfplots}
\usepackage{adjustbox}
\usepackage{emacslstdefaults}
\let\mathdollar\undefined
\lstset{ 
upquote=true,
columns=fullflexible,
basicstyle=\ttfamily,
literate={*}{{\char42}}1
         {-}{{\char45}}1
         {\ }{{\copyablespace}}1
}
\usepackage[space=true]{accsupp}
\newcommand{\copyablespace}{\BeginAccSupp{method=hex,unicode,ActualText=00A0}\hphantom{x}\EndAccSupp{}}

\definecolor{folder} {HTML}{5555FF}
\definecolor{samplefolder}{HTML}{55FF55}
\definecolor{hidden} {HTML}{FF55FF}
\definecolor{tag} {HTML}{00AAAA}


\providecommand{\tabularnewline}{\\}
\newcommand\hindent{\hspace{2em}}
\newcommand\stylefolder[1]{{\bfseries\color{folder}#1}}
\newcommand\stylesamplefolder[1]{{\bfseries\color{samplefolder}#1}}
\newcommand\styletag[1]{{\color{tag}#1}}
\newcommand\stylehidden[1]{{\bfseries\color{hidden}#1}}

\title{SFramework}
\subtitle{Statistics with \texttt{CAF}}

\author[Carsten Burgard]{Carsten Burgard}
\date{24-09-2024}

\setlogo{
  \includegraphics[width=\textwidth]{logos/CAFLogos2024}
}

\tikzset{arrow/.style={->,shorten <=1mm,shorten >=1mm}}

\begin{document}

\section{Introduction}

\maketitle

\begin{frame}{Introduction}
  \begin{itemize}
  \item today, you have learned how to
    \begin{itemize}
    \item produce a sample folder
    \item with histograms and counters
    \item for your nominal analysis
    \item and systematic variations
    \end{itemize}
  \item how can you use that to setup a fit? \par
    $\Rightarrow$ \texttt{SFramework} is the in-house statistics solution
  \end{itemize}
  \begin{block}{Scope}
  \begin{itemize}
  \item this is \textbf{not} an introduction to statistical methods in \acr{HEP}\par
    \textit{you can find that \highlight{\href{https://indico.desy.de/indico/event/19085/timetable}{elsewhere}}}
  \item this will guide you through the neccessary steps to setup a \acr{RooFit}-based workspace \& fit based on your \acr{CAFCore} analyis
  \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Hands-on Setup}
  \begin{itemize}
  \item no specific requirements of release or \acr{ROOT} version
  \item runs with standalone \acr{ROOT} or \acr{ASG} release\par
    \textit{opportunity to see if things work on your laptop?}
  \end{itemize}
  \begin{block}{Setup}
    \vspace{-.5em}
    \begin{adjustbox}{width=\textwidth,minipage=20cm}
    \begin{lstlisting}[language=bash,numbers=none]
mkdir AnalysisExample; cd AnalysisExample
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-caf/CAFExample.git
mkdir build; cd build
cmake ../CAFExample
source setupAnalysis.sh
make -j4
    \end{lstlisting}
    \end{adjustbox}
    \vskip-1em    
  \end{block}
  \begin{alertblock}{Warning}
    \begin{itemize}
    \item You may use the fork of the \texttt{CAFExample} repo you already used
%    \item Due to some last minute changes, need to update your fork
    \end{itemize}
  \end{alertblock}
\end{frame}

%\begin{frame}[fragile]{Hands-on Setup}
%  \begin{alertblock}{Expert git action: fetch from a new git remote}
%    \begin{itemize}
%    \item add a second remote to your git repository, call it \texttt{upstream}
%    \item checkout and pull the \texttt{master} branch from there
%    \item update the submodules
%    \item recompile
%    \item extra: push the update to your fork!
%    \end{itemize}      
%    \begin{adjustbox}{width=\textwidth,minipage=20cm}
%      \begin{lstlisting}[language=bash,numbers=none]
%        cd AnalysisExample
%        git remote add upstream ssh://git@gitlab.cern.ch:7999/atlas-caf/CAFExample.git
%        git pull upstream master
%        git submodule update
%        cafcompile
%        git push origin master
%    \end{lstlisting}
%    \end{adjustbox}    
%  \end{alertblock}
%\end{frame}

\begin{frame}[fragile]{General Workflow}
  \begin{tikzpicture}[y=2cm,x=3cm]
    \node (hist) at (0,-1) {\texttt{histograms}};
    \node at(1,-1) {\includegraphics[width=2cm]{plots/SR}};
    \node (ws) at (0,-2) {\texttt{workspace}};
    \node at (1,-2){\begin{adjustbox}{width=2cm,minipage=60cm}\lstinputlisting{plots/wsprint}\end{adjustbox}};    
    \node (results) at (0,-3) {results};
    \node at(1,-3) {\includegraphics[width=2cm]{plots/likelihood}};
    \draw [arrow] (hist) -- (ws);
    \draw [arrow] (ws) -- (results);
  \end{tikzpicture}
\end{frame}

\begin{frame}{General Workflow -- comments}
  \begin{itemize}
  \item this workflow conceptually splits the process in two parts
    \begin{itemize}
    \item building a statistical model of your data (\texttt{Pdf})
    \item fitting that model to the data by minimizing the $-\!\log(L)$
    \end{itemize}
  \item in-line with \acr{ATLAS} practices, for example, Higgs combination
    \begin{itemize}
    \item analysers give workspace to HComb
    \item fit is run by people in HComb
    \end{itemize}
  \item workspace building is completely decoupled from the fitting itself, can happen with different frameworks/tools
    \begin{itemize}
    \item use \texttt{SFramework} to build your workspace\par\footnotesize
      \textit{highly encouraged: \texttt{SFramework} can re-use much of the existing information in your sample folder!}
    \item use any fitting tool you like (but also \texttt{SFramework}) to run your fit\par\footnotesize
      \textit{\texttt{SFramework} provides many features and convenient configuration, but other statistics frameworks are also very popular!}
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Workspace building}

\begin{frame}[shrink=10]{Workspace building with \texttt{SFramework}}
  is segmented into several distinct tasks (or \texttt{Actions})
  \begin{enumerate}
  \item \textbf{define your model}
    \begin{itemize}
    \item Which signal and background samples exist? Include data?\par
      $\rightarrow$ \textit{paths in your sample folder}
    \item Which regions/channels do we include in the fit?\par
      $\rightarrow$ \textit{cuts and histogram names in your sample folder}
    \item Which systematic variations exist, and how are they defined?\par
      $\rightarrow$ \textit{systematics included in your sample folder}
    \end{itemize}
  \item post-process your model (optional)
    \begin{itemize}
    \item prune and sanitize your systematics
    \item include additional theory systematics
    \item more fancy things
    \end{itemize}
  \item \textbf{create a workspace} from your model
  \item post-process the workspace (optional)
    \begin{itemize}
    \item reparametrize parts of your Pdf
    \item rename, fix, remove or merge parameters
    \end{itemize}
  \item \textbf{write out the workspace} to a file
  \end{enumerate}
\end{frame}


\begin{frame}[fragile]{Primer: \texttt{Actions}}
  \begin{itemize}
  \item \texttt{SFramework} is steered with config files (surprise!)
  \item the config files have the \texttt{TQFolder} syntax (next surprise!)
    \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]
# -*- mode: tqfolder -*-
+DoSomething {
  +WithObject {
    # ... how to do it ...
  }
}
+DoTheNextThing {
  +WithObject;
}
  \end{lstlisting}
\item use TQFolder syntax to compactify your config files!
\item actions executed in order given -- for two calls to the same action, suffix it with \texttt{.somename} (\texttt{+DoSomething.first})
  \end{itemize}
\end{frame}

\begin{frame}[fragile,shrink=10]{Step 1: \href{https://gitlab.cern.ch/atlas-caf/CAFExample/-/blob/master/share/sframework/minimal/build.txt}{\texttt{CreateModels} \ExternalLink} -- mini-example}
  \vspace{-2.5em}
  \begin{lstlisting}[language=tqfolder]
+CreateModels {
  +MyAnalysis { # model name
    +Samples {
      +Signal     { <Type=S, Path="sig"> }
      +Background { <Type=B, Path="bkg"> }      
    }
    +Channels {
      +SR {  <Histogram = "CutSR/MyVar">  }
      +CR {  <Counter = "CutCR">          }
    }
    +Variations {
      <SampleFolder="samples.root:samples">
      +Nominal {  <variation=""> }
    }
  }
}
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Sample folder layout}
  \begin{itemize}
    \item snatch the input sample folder\par
      \begin{adjustbox}{width=\textwidth,minipage=20cm}      
        \texttt{/eos/user/a/atlascaf/tutorial/2021\_Apr/sframework/samples.root}
      \end{adjustbox}
  \end{itemize}
  \begin{center}
  \begin{adjustbox}{width=.8\textwidth}
  \begin{tabular}{l l l}
\bfseries Name & \bfseries Class & \bfseries Details\tabularnewline\hline\hline
\stylesamplefolder{sig}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 50 +/- 0.707107, N=5000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1 +/- 0.0001 [raw: 0]\tabularnewline
\stylesamplefolder{bkg}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 500 +/- 7.07107, N=5000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1000 +/- 1 [raw: 0]\tabularnewline
\stylesamplefolder{data}/ & TQSampleFolder & 2 objects, no tags\tabularnewline
\hindent{}\stylehidden{.histograms}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\stylefolder{CutSR}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}\hindent{}MyVar & TH1F & (-1 ... 1, 11 bins), S(w) = 553 +/- 23.516, N=10000\tabularnewline
\hindent{}\stylehidden{.cutflow}/ & TQFolder & 1 object, no tags\tabularnewline
\hindent{}\hindent{}CutCR & TQCounter & 1000 +/- 31.6228 [raw: 0]\tabularnewline
  \end{tabular}
  \end{adjustbox}
  \end{center}  
  \end{frame}

\begin{frame}[fragile,shrink=10]{Step 1: \href{https://gitlab.cern.ch/atlas-caf/CAFExample/-/blob/master/share/sframework/minimal/build.txt}{\texttt{CreateModels} \ExternalLink} (cont'd)}
  \begin{itemize}
  \item this model is extremely minimal
    \begin{itemize}
    \item only two samples (\texttt{Signal}, \texttt{Background})
    \item no systematics are added (not even \acr{MC} stat uncertainties)
    \item no parameters -- not even \acr{NF}s (\textbf{cannot fit this yet!})
    \end{itemize}
  \item let's add a normalization factor
    \vspace{-2em}
    \begin{lstlisting}[language=tqfolder,numbers=none]
+Signal {      
  <Type=S, Path="sig">
  +NormFactor.mu {
    <Val=1.,Low=-50,High=50.,Const=false>
  }
}
    \end{lstlisting}
  \item let's also add \acr{MC} stat uncertainties for background
    \vspace{-2em}
    \begin{lstlisting}[language=tqfolder,numbers=none]    
+Background {
 <Type=B,Path="bkg",ActivateStatError=true>
}
    \end{lstlisting}
    \item can you also add a background NF?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Primer: Run it!}
  \begin{block}{Execute}
    \texttt{python statistics.py myconfig.txt}
  \end{block}
  \begin{block}{Output}
    \vspace{-.5em}
  \begin{adjustbox}{minipage=20cm,width=\textwidth}
\begin{lstlisting}[numbers=none]
starting statistical treatment of the analysis results
INFO: Imported config file 'myconfig.txt'
INFO: Starting Statistics Manager on 'myconfig.txt'
INFO in TSStatisticsManager::run(...) :  === CreateModels: CreateModels === 
SFramework/TSStatisticsManager: Creating model 'MyAnalysis' 
SFramework/TSModelBuilder: prepareModel(): Preparing model...
SFramework/TSModelBuilder: createDefinition(): Creating definition for model 'MyAnalysis' ...
SFramework/TSModelBuilder: collectAllHistograms(): Start collecting histograms...
SFramework/TSModelBuilder: collectHistograms('Nominal'): Collecting histograms...
SFramework/TSSystematicsManager: includeAllSystematics(): Start including systematics ...
SFramework/TSSystematicsManager: processAllSystematics(): Start processing systematics matching '*'
INFO: Done processing 'myconfig.txt'!
\end{lstlisting}
  \end{adjustbox}
  \end{block}
\end{frame}


\begin{frame}[fragile]{What about the workspace?}
  \begin{itemize}
  \item we didn't actually build a workspace yet -- just a model
  \item we need to turn the model into a workspace
  \end{itemize}
    \vspace{-2em}
\begin{lstlisting}[language=tqfolder,numbers=none]    
+CreateWorkspaces {
  +MyAnalysis; # model & workspace name
}
+ExportWorkspaces {
  +MyAnalysis { # workspace name
    <outputFile="workspace.root">
  }
}
\end{lstlisting}
\begin{block}{Writing \texttt{JSON} files for publishing at \texttt{HEPdata}}
Export a workspace as \texttt{JSON} by using file extension \texttt{.json}
\end{block}
\end{frame}

\begin{frame}[fragile]{Did it work?}
  \begin{block}{Open root\dots}
    \vspace{-2.5em}
    \begin{lstlisting}[language=bash,numbers=none]
      root workspace.root
    \end{lstlisting}
  \end{block}
  \begin{block}{Run a simple fit}
    \begin{adjustbox}{minipage=15cm,width=\textwidth}    
    \vspace{-1em}
      \begin{lstlisting}[language=rootc++,numbers=none]      
        RooAbsPdf* pdf = MyAnalysis->pdf("simPdf")
        RooAbsData* thedata = MyAnalysis->data("asimovData")
        pdf->fitTo(*thedata)
      \end{lstlisting}
    \end{adjustbox}
    \begin{itemize}
      \item that was easy! but how do we run a fit with \texttt{SFramework}?
    \end{itemize}
  \end{block}
\end{frame}

\section{Running fits}

\begin{frame}[fragile]{Running Fits with \texttt{SFramework}}
  \begin{itemize}
  \item let's \highlight{\href{https://gitlab.cern.ch/atlas-caf/CAFExample/-/blob/master/share/sframework/minimal/run.txt}{run a fit \ExternalLink}} with SFramework
  \end{itemize}
  \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]
+ImportWorkspaces {
 +MyAnalysis { <inputFile="workspace.root"> }
}
+Fit {
  +MyAnalysis{
    <dataset="asimovData">
  }
}
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile,shrink=10]{How about a likelihood scan?}
  \begin{itemize}
  \item profile likelihood scans are profiled, that is,
    \begin{itemize}
    \item a likelihood is scanned over a certain range in the \acr{POI}
    \item for each point, the \acr{POI} is fixed to the coordinates of that point
    \item all other parameters are fitted to the data
    \item value of the likelihood after minimization is shown as a function of the \acr{POI} value
    \end{itemize}
  \item can be very time-consuming to run locally
  \item but for our simple model, this should run within seconds!
  \end{itemize}
  \vspace{-2em}
  \begin{lstlisting}[language=tqfolder]  
+ScanLikelihood.mu{
  +MyAnalysis {
    <datasetName="asimovData">
    +mu{
      <min=0,max=3.5, nbins=100>
    }
  }
}
  \end{lstlisting}
\end{frame}


\section{Results}

\begin{frame}[fragile]{How do we get the results?}
  \begin{itemize}
  \item just "running a fit" is not so useful
  \item we want to \highlight{\href{https://gitlab.cern.ch/atlas-caf/CAFExample/-/blob/master/share/sframework/minimal/plot.txt}{visualize the results \ExternalLink}}
  \item \texttt{SFramework} stores all the results in a TQFolder
  \begin{lstlisting}[language=tqfolder]      
+ExportResults {
  +MyAnalysis {
    <outputFile = "result.root">
  }
}
  \end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Plotting the Likelihood?}
  \begin{itemize}
  \item you can pick up the result and plot it
  \item there is an action for this
  \end{itemize}
  \begin{adjustbox}{minipage=18cm,width=\textwidth}
  \begin{lstlisting}[language=tqfolder]      
+ImportResults {
  +MyAnalysis {
    <inputFile = "result.root:MyAnalysis">
  }
}
+PlotLikelihoodScan {
 +MyAnalysis {
  <arrangement = "$TQPATH/../SFramework/share/templates/PlotLikelihoodScanSignificance.txt">
  <outputFile="likelihood.pdf">
  +mu_asm {
    <source = "LikelihoodScans/ScanLikelihood.mu/Scan">
    <style.lineColor=kRed, style.title="#mu">
  }
  <style.title.xAxis = "#mu (profiled)">
 }
}
  \end{lstlisting}
  \end{adjustbox}
\end{frame}

\begin{frame}{Plotting the Likelihood}
  \includegraphics[width=\textwidth]{plots/likelihood}
\end{frame}

\section{Systematics}

\begin{frame}[fragile]{Systematic uncertainties}
  \begin{itemize}
  \item in our little playset of a model, we don't have any systematics
  \item in a realistic model, adding systematics would look like this
  \end{itemize}
  \begin{adjustbox}{minipage=24cm,width=\textwidth}  
  \begin{lstlisting}[language=tqfolder]        
+CreateModels {
  +MyAnalysis { # model name
    ...
    +Samples {
      +Signal     { <Type=S, Path="sig/all$(variation)"> }
      +Background { <Type=B, Path="bkg/all$(variation)"> }
    }
    +Variations {
      +Nominal {  <variation=""> }
      +MySystematic1_up {  <variation="_mySystematic1_up"> }
      +MySystematic1_down {  <variation="_mySystematic2_dn"> }        
    }
    +Systematics {
      +MySystematic1 { <Up="MySystematic1_up", <Down="MySystematic1_down"> }
    }
  }
}
  \end{lstlisting}
  \end{adjustbox}
  \begin{itemize}
  \item no systematics in the sample folder in the playground model
  \item in reality, \texttt{+Variations} and \texttt{+Systematics} separated out into a different file that can be \texttt{\$include()}d
  \end{itemize}    
\end{frame}

\begin{frame}[fragile]{Systematic uncertainties (cont'd)}
  \begin{itemize}
  \item for some theory systematics, there's actually some math to be done when constructing the systematics from the variations
  \end{itemize}
  \begin{adjustbox}{minipage=10cm,width=.55\textwidth}    
    \begin{lstlisting}[language=tqfolder]
+Systematics {
    +theo_pdf {
       <Up="pdf_up">
       +Compute/Up{
         <Mode="Hessian", Baseline="pdf_0">
         <Variations={"pdf_1", ..., "pdf_30" }>
       }
    }
}
  \end{lstlisting}
  \end{adjustbox}
  \begin{adjustbox}{minipage=10cm,width=.4\textwidth}
  \begin{block}{Sample folders}
    \begin{itemize}
    \item in real life, theory uncertainties are often given as separate sample folders (``sample folder method'')
    \item \texttt{SFramework} can be instructed to obtain inputs for some variations from dedicated sample folders
    \item can be enabled by putting a line like \lstinline[language=tqfolder]{<SampleFolder="samples_theo.root:samples" @ Variations/theo_pdf*;}
    \end{itemize}      
  \end{block}
  \end{adjustbox}    
  \begin{itemize}
  \item use \texttt{+Compute/Up} to compute the ``Up'' variation on the fly 
  \item the (missing) ``Down'' variation is obtained by symmetrizing
  \end{itemize}
\end{frame}  


\begin{frame}[fragile]{Systematic uncertainties, Processing of}
  \begin{itemize}
  \item most time working on fits is spent understanding and debugging systematics
  \item \texttt{SFramework} has an action \texttt{+ProcessSystematics} as a swiss-army-knife for systematics
  \item example: prune all experimental systematics below 0.1\% effect or above 80\% on region yields
  \end{itemize}
  \begin{adjustbox}{minipage=24cm,width=\textwidth}      
  \begin{lstlisting}[language=tqfolder]  
+ProcessSystematics.prune_normalization_systematics { # we'll have several of these, so let's give them names
  +MyAnalysis {
    <select.Systematics={"*"}> # select all systematics matching the pattern
    <except.Systematics={"theo_bkg_*","theo_sig_*"}> # exclude all systematics matching the pattern
    <select.SystematicsTypes={"OverallSys"}> # could also be "HistoSys" for shapes
    <SysRelThreshold = 0.001>  # prune (remove) all systematics below a threshold of .1% (they don't matter)
    <SysRelCutoff = 0.8> # prune (remove) all systematics above a threshold of 80% (they are likely non-sensical)
  }
}
  \end{lstlisting}
  \end{adjustbox}
\end{frame}

\begin{frame}[fragile]{Systematic uncertainties, Processing of (cont'd)}
  \begin{itemize}
  \item problem: shape uncertainties are often sensitive to fluctuations in MC statistics
  \item possible solution: smooth shape uncertainties
  \item many different smoothing algorithms available, use \texttt{CommonSmoothingTool} implementation
  \end{itemize}
  \begin{adjustbox}{minipage=24cm,width=\textwidth}      
    \begin{lstlisting}[language=tqfolder]
+ProcessSystematics.smooth {
  +HWW_ggFVBF_01jDF {
    <histoSys.smooth = true> # perform smoothing
    <select.Systematics = {"*"}> # on all systematics
    <histoSys.smoothingMethod = "smoothTtresDependent"> # use TRexFitter smoothing alg
    <histoSys.smoothDirectionAndSymmetrize = "Up"> # use the Up direction
    <histoSys.prune = true> # also do pruning
    <histoSys.flatChi2pValMin = 0.05> # prune all systematics that are compatible with a flat line
    <verbose=false> # don't spam too many messages
    <select.SystematicsTypes={"HistoSys"}> # only do shapes
  }
}
  \end{lstlisting}
  \end{adjustbox}
\end{frame}


\begin{frame}[fragile]{Hands on: Systematic uncertainties, Adding fake}
  \begin{itemize}
  \item there are no systematics in our playground analysis, invent some
  \item you can modify the numbers below
  \end{itemize}
  \begin{adjustbox}{minipage=18cm,width=\textwidth}      
    \begin{lstlisting}[language=tqfolder]
+Systematics {
  +MySystematic1 {
    <Up="MySystematic1_Up">
    +Compute/Up {
      <Mode="Slope", Baseline="Nominal", slope=.1, scale=1.0, randomize=1.>
    }
  }
  +MySystematic2 {
    <Up="MySystematic2_Up">
    +Compute/Up {
      <Mode="Norm", Baseline="Nominal", scale=1.15, randomize=1.>
    }
  } 	
}
    \end{lstlisting}
  \end{adjustbox}
  \begin{itemize}
  \item this code only creates the ``Up'' variations -- what do you reckon happens with ``Down''?
  \end{itemize}  
\end{frame}

\begin{frame}[fragile]{Hands on: Hacking the model}
  \begin{itemize}
  \item sometimes, need exercise very granular control over the model
  \item most common example: decorrelate normalization and shape effects of a nuisance parameter
  \item since the model is just a \texttt{TQFolder}, you can use your \texttt{TQFolder} expertise to modify it
  \item the below code decorrelates normalization and shape effects of \texttt{MySystematic1} for all samples in the signal region
  \item can you change it to also affect the control region?
  \end{itemize}
  \begin{adjustbox}{minipage=18cm,width=\textwidth}      
    \begin{lstlisting}[language=tqfolder]
+EditModels.decorrelate_ShapeSys1 {
  +MyAnalysis {        
    <commands = {
      "@Channel.*SR*/Sample.* { $move(HistoSys.MySystematic1>>::HistoSys.MySystematic1_shape); }",
      "@Channel.*SR*/Sample.* { $move(OverallSys.MySystematic1>>::OverallSys.MySystematic1_norm); }"
    }>
  }
}
    \end{lstlisting}
  \end{adjustbox}
\end{frame}

\begin{frame}[fragile]{Hands on: Getting out some histograms}
  \begin{itemize}
  \item as particle physicists, also want to look at distributions
  \item want to turn statistical model back into a histogram stack plot
  \item this snippet will get you two \texttt{.root} files with histograms
  \end{itemize}
  \begin{adjustbox}{minipage=18cm,width=\textwidth}      
    \begin{lstlisting}[language=tqfolder]
+ExportHistograms{
  +MyAnalysis {
    +postfit {
      <snapshot="SnSh_AllVars_Unconditional_asimovData">
      <outputFile="histograms-postFit.root">
      <dataName="asimovData">
      <fitResult="Fit/Fit">
    }
    +prefit {
      <snapshot="SnSh_AllVars_Nominal">
      <outputFile="histograms-preFit.root">
      <dataName="asimovData">
    }    
  }
}
    \end{lstlisting}
  \end{adjustbox}
\end{frame}

\begin{frame}[fragile]{Hands on: Plotting some histograms}
  \begin{itemize}
  \item unless you fancy looking at histograms in the \texttt{TBrowser} (no shame!), there's also a convenient plotting script
  \end{itemize}
    \vspace{-.6cm}
    \begingroup
  \footnotesize
    \begin{lstlisting}[language=tqfolder]
python ../tools/visualizeFitInputs.py --unblind -o myplots \
      -i ../../output/histograms-postFit.root 
    \end{lstlisting}
    \endgroup
    
    \hfil\includegraphics[width=.3\textwidth]{plots/SR-raw}\hfil    \includegraphics[width=.3\textwidth]{plots/SR}    \hfil
  \begin{itemize}
  \item improve the styling using a process file and some options
  \end{itemize}

  \vspace{-.8cm}
  \begingroup
  \footnotesize  
    \begin{lstlisting}[language=tqfolder]    
      --options geometry.sub.xAxis.titleOffset=3 \
      --processes sframework/minimal/snippets/postfit-histograms.txt 
    \end{lstlisting}
    \endgroup
\end{frame}


\section{Recap \& Hands-on}

\begin{frame}[shrink=10]{Recap}
  \begin{itemize}
  \item \texttt{SFramework} knows 3 types of objects:
    \begin{description}
    \item [model] TQFolder encoding a statistical model
    \item [workspace] A \texttt{RooFit} workspace
    \item [result] TQFolder encoding a statistical result
    \end{description}
  \item \texttt{SFramework} performs \texttt{Action}s on these objects
    \begin{itemize}
    \item can take objects as inputs
    \item can produce objects as outputs
    \item can have side effects (files written, etc.)
    \end{itemize}
  \item ``Is there an \texttt{Action} that can \dots''
    \begin{itemize}
      \item very likely answer: yes, see \href{https://atlas-caf.web.cern.ch/SFrameworkActions/index.html}{\highlight{documentation}}
    \end{itemize}
  \end{itemize}
  \begin{block}{Hands-on: Tasks}
    \begin{itemize}
    \item can you reproduce the Likelihood scan? 
    \item how large is the expected significance of this toy analysis?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{FAQ \& Gotchas}
  \begin{itemize}
  \item you only need the \texttt{Import...} and \texttt{Export...} actions if you actually split your configuration into different files
    \begin{itemize}
      \item if you use them nevertheless, you will see error messages, but everything will still run as intended
    \end{itemize}
  \item for exporting histograms, you need both a \texttt{result} and a \texttt{workspace}
  \end{itemize}
\end{frame}

\section{Reality Check}

\begin{frame}[fragile]{That was easy, what now?}
  \begin{itemize}
  \item in reality, things are often more involved
  \item big factor: systematics
  \item inputs get very large, model building becomes slow\par
    $\Rightarrow$ split your config into steps to minimize reprocessing
  \item fits become very slow\par
    $\Rightarrow$ parallelize using a batch system, merge back results
  \item you can look at a few realistic examples in the \texttt{\href{https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode/-/tree/master/share/config/statistics/ggFVBF-01j-DF?ref_type=heads}{HWWAnalysisCode}} package
  \end{itemize}
\end{frame}

\begin{frame}{Outlook}
  \begin{itemize}
  \item with \texttt{SFramework}, you should be able to setup and run the fit for most standard analyses
  \item extra packages can be integrated to provide bonus functionality
    \begin{itemize}
    \item improve your fit stability and get additional workspace manipulation magic by including \texttt{\href{https://gitlab.cern.ch/cburgard/RooFitUtils}{RooFitUtils}}
    \item get additional \texttt{RooFit} classes to upgrade your setup to a full \texttt{HComb ROOT} release by including \texttt{\href{https://gitlab.cern.ch/cburgard/RooFitExtensions}{RooFitExtensions}}
    \end{itemize}
  \item there are lots of additional actions we did not cover here
    \begin{itemize}
    \item combine workspaces just like it's done in \texttt{HComb} 
    \item lots of plotting and printing for detailed studies on how your systematics behave
    \item modify your workspace to perform regularized profile likelihood unfolding
    \item \dots
    \end{itemize}      
  \end{itemize}
\end{frame}

\end{document}
