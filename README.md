# CAF Slides



## What is this?

This is a repository for holding slides used for CAF tutorials.

## How can I help?

It would be wonderful if you'd add your slides, in an editable format! This could include powerpoint, latex, a link (giving editing rights!) to google slides, Word (please no), or whatever else you use. 

Since you (used to) work with CAF, we trust that you are organized, tech-savvy and capable of adding your contributions to this repo in a way that makes sense. In case that isn't fully the case, we'd request:

* links go into the document called "links"
  * Please give us some indication of what they're for and of who you are, eg. `TQFolder syntax (Carla Cafster): https://my.google.slides.com`
* slides go into the folder called "slides"
  * Please give us some indication of what they're for and of who you are, eg. by naming them `CarlaCafster-TQFolderSyntax.ppt`
* feel free to make additional folders for other types of contributions
  * Please give us some indication of what they're for and of who you are, eg. by naming the folder `CarlaCafster-TQFolderSyntax-texFiles`

## How can I go above and beyond?

If you are still involved in CAF, please add your current institute's logo to the folder called `logos`!
